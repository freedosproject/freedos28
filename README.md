# FreeDOS 28th Anniversary Ebook

A year ago, I reached out to FreeDOS users and developers to ask why they love FreeDOS. We've collected these stories into a PDF ebook, which you can read for free, here: Why We Love FreeDOS (pdf).

Thanks to everyone who responded to interviews for this ebook. I also want to thank everyone who is - or has been - part of the FreeDOS Project since 1994. You are awesome!

This ebook is available under the terms of the Creative Commons Attribution-ShareAlike 4.0 International license.

Actually released ("published") July 2023, which actually makes this the "29th" anniversary celebration instead. (*I've renamed the file to "29" instead.*)
