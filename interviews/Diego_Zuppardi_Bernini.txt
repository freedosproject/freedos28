> What is your earliest/first memory of using DOS?

Well, my first contact was in the late 80s, at my mother’s job
initially only as a launcher tool (floppy disks were used), then in
early 90s I started learning and using DOS 3.3.  I founded easy to use
and far better than BASIC from Home Computers.

At high school they had some MSX computers, so I used MSX DOS too.

I quickly moved to DOS 5 as soon as I really started using it for real
work and for better hardware support.

> How did you first get interested in FreeDOS?

Mid in 90s I was always looking for free (open source) os and I found
FreeDOS I was very happy because I was upset about the DOS end of life
(I know it was there hidden in Windows 95 but I felt that it was not
the same).

The first thing I did was testing my Clipper’s apps, then I tried to
run Win 3.1 but only for curiosity.

> How do you run FreeDOS today?

Nowadays I mainly use FreeDOS on a VM, the typical set up is 32 MB
RAM, 2GB HD, network and sound card enabled.

I love restoring old computers and I use FreeDOS and OpenGEM on most
of them.

> What's cool about FreeDOS 1.3? What features do you use the most?

There are a lot of cool stuff in 1.3 release, as networking, 8086 FAT
32 support, new command line interpreter, new kernel, tons of apps and
utilities, etc.

I use fdimples, developer tools like I16GCC and Open Watcom, Internet
text browsers, and of course ZXSpectrum emulators (my favorite 8 bit
computer)

> Why is FreeDOS important to you? Why do you run FreeDOS in 2022?

FreeDOS and in general open source OS’s and tools are fundamental to
give opportunities to all people to use, learn and develop skills in
the digital world.

It provides a fundamental basement to be more democratic and fairer,
especially for people otherwise don’t have access to the software
licenses and appropriate training.

It also give the opportunity to lean and experiment even I you aren’t
in IT field, or you have few o none computing skills.

No less important, open source OS are more secure, because you can
audit, improve or modify as you need.

Finally, FreeDOS also demonstrates that if good will people work
together wonderful things could happen, you and the team make a great
gift to all of us, giving the opportunity to work, lean or whatever we
want or need with the OS.

> What programs do you like to run on FreeDOS?

I like running a lot of programs for testing or daily use such as
compilers, spreadsheets, database and hardware info systems.

I enjoy many apps. I really don’t have a favorite one, but nowadays if
I must carry only one program, I definitely choose a C compiler.

> To indicate you agree to your content being included under the
> Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA
> 4.0) license, please type "I AGREE" here:

I AGREE

> Please type your name EXACTLY as you would like it spelled in the
> book:

Diego Zuppardi Bernini
