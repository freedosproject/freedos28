Call me Gregory Pietsch on the interview questions and bear in mind
with the software I write that I want to expand the limits of what
FreeDOS can do.

> What is your earliest/first memory of using DOS? 

I started using MS-DOS in 1983. I was still a teenager and my father
purchased a few PC clones from a company called Corona, and I trained
myself to run programs on it. When I got to college, I had a ‘286 that
I kept upgrading until I couldn’t do it anymore, and it ran MS-DOS.

> How did you first get interested in FreeDOS?

I stumbled upon FreeDOS about 1998. I thought at the time, free MS-DOS
clone, it must be a fun project. The first thing I did was take a look
at the code on the website, taking a mental note about things that
could be improved upon or things that were missing.

> How do you run FreeDOS today?

To be honest, I don’t run FreeDOS all that much. (The last time I
tried, it was as a VM.) But I am vested in the success of the
operating system, and I am nostalgic about DOS. DOS was my go-to
operating system for twenty years or thereabouts. Microsoft has not
given away the source code to MS-DOS, or made it free and thus
improvable by others, so FreeDOS is the only real way I or anyone else
can take this clunky operating system and improve it.

> What's cool about FreeDOS 1.3? What features do you use the most?

The cool thing about FreeDOS is that there are many diehards out there
that have contributed to the success of FreeDOS over the years. In
regard to the features, I have attempted in my software to get the
best out of the operating system with each program or library I write.

> Why do you run FreeDOS in 2022?

Why not? It’s awesome!

> What programs do you like to run on FreeDOS? Do you have a favorite app,
> or favorite game? If you could take only one DOS program with you to a
> desert island, what would it be?

Why would I take a personal computer to a place that does not have
electricity?

> What's the coolest program you've written for DOS?

I have written many cool programs over the years. Could I mention this
line editor that I think is awesome?

> What program/s have you written for FreeDOS?

First of all, there’s FreeDOS Edlin. I wrote it because I noticed a
vacuum in the FreeDOS distribution and decided to fill it, hoping to
get fame and (possibly) fortune along the way. I have also written a
version of ar, the Unix archiver, as well as a few other clones of
Unix programs. I also contributed a few libraries such as a version of
libm and a multi-precision integer math library. I wrote those because
I wanted to write a C compiler (I already had an editor written) and
it turned out to be too much of an undertaking to write everything
that would go with the compiler: the compiler itself, the binary
utilities, the development utilities, and the libraries. There was a
libc clone already, but not a libm clone. Anyway, I wrote a few of the
things I would need for this before my brain exploded, and thought
somebody could reuse them, so I sent them in.

> What's different about writing programs for DOS than another
> operating system like Windows or Linux?

DOS is a single-tasking operating system that’s barely more than a
memory. It only exists so that you can run other programs on top of
it. Graphics tend to bog down the CPU, so the best programs you can
run on DOS are simple filters such as Edlin. If you have to do
windowing on a 25-by-80 screen or access the serial ports of the PC,
the best way was to poke values into absolute addresses or peek at
those values. For Windows and Linux, you can’t really do that. Native
Windows code relies on a complicated event-driven model of input and
output. Linux, like Unix, has its own quirks.

To write programs for DOS, remember the KISS principle. Dynamically
allocate your memory instead of using a giant pre-allocated
block. Make it so portable that it compiles and runs on Linux or
Unix. Remember what I wrote in the previous paragraph about simple
filters.

